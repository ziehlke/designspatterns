package com.patterns.operational.carDecorator;

public interface Car {
    void enter();
    void drive();
    void stop();
}
