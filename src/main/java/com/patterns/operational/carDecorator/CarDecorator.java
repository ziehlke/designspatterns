package com.patterns.operational.carDecorator;

public abstract class CarDecorator implements Car {
    Car component;

    protected CarDecorator(Car car) {
        component = car;
    }


    @Override
    public void enter() {
        component.enter();
        System.out.println("Siadasz na fotelu kierowcy.");
    }

    @Override
    public void drive() {
        System.out.println("Wkładasz kluczyk do stacyjki.");
        component.drive();
    }

    @Override
    public void stop() {
        component.stop();
        System.out.println("Zrobiłeś trzy okrążenia po parkingu - w końcu jest wolne miejsce.");
    }
}
