package com.patterns.operational.carDecorator;

public class BmwTruck extends CarDecorator {
    protected BmwTruck(Car car) {
        super(car);
    }

    public BmwTruck() {
        this(new Bmw());
    }

    @Override
    public void enter() {
        component.enter();
    }

    @Override
    public void drive() {
        component.drive();
    }


    @Override
    public void stop() {
        System.out.println("Potrzebujesz duuuuużego miejsca");
        component.stop();
        System.out.println("Ale i tak się tam nie zmieścisz... robisz kolejne okrążenia.");
    }
}
