package com.patterns.operational.observer;

public interface Observer {
    void update(String name);
}
