package com.patterns.operational.observer;

public class NewspaperFakt extends Subject {

    public NewspaperFakt() {
        super("Fakt");
    }


    public String getName() {
        return name;
    }

}
