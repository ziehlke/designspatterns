package com.patterns.operational.composite;

import java.util.ArrayList;
import java.util.List;

public class Composite extends Component {

    private List<Component> components;
    private Component child;


    public Composite(int value) {
        super(value);
        components = new ArrayList<>();
        child = null;
    }

    @Override
    public void printValue() {
        System.out.println(this.value);
    }


    public void addComponent(Component component) {
        child = component;
        components.add(component);
    }


    public void removeComponent(Component component) {
        components.remove(component);
    }

    public Component getChild() {
        return child;
    }

}