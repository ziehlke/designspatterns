package com.patterns.operational.bridge;

public class PaintApi {
    public void drawALine() {
        System.out.println("Paint draw a line.");
    }

    public void drawACircle() {
        System.out.println("Paint draw a circle.");
    }
}
