package com.patterns.operational.bridge;

public class Paint extends Drawing {

    private PaintApi paintApi;

    public Paint() {
        paintApi = new PaintApi();
    }


    @Override
    public void drawLine() {
        paintApi.drawALine();

    }

    @Override
    public void drawCircle() {
        paintApi.drawACircle();
    }
}
