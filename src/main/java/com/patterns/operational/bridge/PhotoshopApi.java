package com.patterns.operational.bridge;

public class PhotoshopApi {

    public void drawALine() {
        System.out.println("Photoshop draw a line.");
    }

    public void drawACircle() {
        System.out.println("Photoshop draw a circle.");
    }


}
