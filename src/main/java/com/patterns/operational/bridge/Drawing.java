package com.patterns.operational.bridge;

public abstract class Drawing {

    public abstract void drawLine();
    public abstract void drawCircle();


}
