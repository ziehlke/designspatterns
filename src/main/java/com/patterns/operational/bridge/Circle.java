package com.patterns.operational.bridge;

public class Circle extends Shape{
    public Circle(Drawing drawing) {
        super(drawing);
    }

    @Override
    public void draw() {
        this.drawCircle();
    }
}
