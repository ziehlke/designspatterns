package com.patterns.operational.adapter;

public class Wizzard {
    private String name;
    private int intelligence;
    private int speed;

    public Wizzard() {
        this.name = "Gandalf";
        this.intelligence = 1000;
        this.speed = 1;
    }

    public void castDestructionSpell(){
        System.out.println(this.name + " cast a spell and giving: " + this.intelligence + " dmg.");
    }

    public void run() {
        System.out.println(this.name + " run away from the battelfield.");
    }

    public void defend() {

        System.out.println(this.name + " blocks attack with staff.");
    }

}
