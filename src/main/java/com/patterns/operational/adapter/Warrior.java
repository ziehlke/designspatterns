package com.patterns.operational.adapter;

public class Warrior {
    private String name;
    private int strengh;
    private int speed;


    public Warrior() {
        this.name = "Aragorn";
        this.strengh = 120;
        this.speed = 10;
    }

    public void swingSword() {
        System.out.println(this.name + " swings his sward giving: " + this.strengh + " dmg.");
    }

    public void defendUsingShield() {
        System.out.println(this.name + " defended himself with his shield.");
    }

    public void run() {
        System.out.println(this.name + " never leaves battelfield.");
    }


}
