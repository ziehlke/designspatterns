package com.patterns.creational.excercises.abstractFactory.prototype;

public class CellPrototype implements Cloneable {
    private int mass;


    public CellPrototype(int mass) {
        this.mass = mass;
    }


    public CellPrototype cloneCell() throws CloneNotSupportedException {
        return (CellPrototype) this.clone();
    }

    public void setMass(int mass) {
        this.mass = mass;
    }
}
