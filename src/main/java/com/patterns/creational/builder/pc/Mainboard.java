package com.patterns.creational.builder.pc;

public enum Mainboard {
    Asus,
    Gigabyte,
    Radeon,
    MSI

}
