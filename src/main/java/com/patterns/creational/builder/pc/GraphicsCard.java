package com.patterns.creational.builder.pc;

public enum GraphicsCard {
    Gigabyte,
    MSI,
    Zotac,
    EVGA,
    ASUS
}
