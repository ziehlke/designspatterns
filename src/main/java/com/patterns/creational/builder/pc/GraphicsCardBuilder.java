package com.patterns.creational.builder.pc;

public interface GraphicsCardBuilder {
    CaseBuilder setGraphicalCard(GraphicsCard card);
}


// GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->