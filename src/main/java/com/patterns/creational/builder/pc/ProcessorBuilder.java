package com.patterns.creational.builder.pc;

public interface ProcessorBuilder {
    MainboardBuilder setProcessor(Processor processor);
}

// GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->