package com.patterns.creational.builder.pc;

public class PersonalComputer {
    private Mainboard mainboard;
    private Processor processor;
    private Drive drive;
    private RAM ram;
    private GraphicsCard graphicsCard;
    private Case pcCase;


    public void setMainboard(Mainboard mainboard) {
        this.mainboard = mainboard;
    }

    public void setProcessor(Processor processor) {
        this.processor = processor;
    }

    public void setDrive(Drive drive) {
        this.drive = drive;
    }

    public void setRam(RAM ram) {
        this.ram = ram;
    }

    public void setGraphicsCard(GraphicsCard graphicsCard) {
        this.graphicsCard = graphicsCard;
    }

    public void setPcCase(Case pcCase) {
        this.pcCase = pcCase;
    }
}
