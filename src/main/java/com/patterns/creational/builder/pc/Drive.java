package com.patterns.creational.builder.pc;

public enum Drive {
    HDD,
    SDD,
    SHDD
}
