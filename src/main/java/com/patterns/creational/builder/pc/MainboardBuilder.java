package com.patterns.creational.builder.pc;

public interface MainboardBuilder {
    GraphicsCardBuilder setMainboard(Mainboard mainboard);
}

// GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->