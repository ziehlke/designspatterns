package com.patterns.creational.builder.pc;

public enum Processor {
    Intel,
    AMD
}
