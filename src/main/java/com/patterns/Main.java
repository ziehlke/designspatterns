package com.patterns;

import com.patterns.creational.builder.pc.*;
import com.patterns.creational.excercises.abstractFactory.*;
import com.patterns.creational.excercises.abstractFactory.prototype.CellPrototype;
import com.patterns.operational.bridge.Circle;
import com.patterns.operational.bridge.Paint;
import com.patterns.operational.bridge.Photoshop;
import com.patterns.operational.bridge.Rectangle;
import com.patterns.operational.carDecorator.*;
import com.patterns.operational.command.*;
import com.patterns.operational.composite.Component;
import com.patterns.operational.composite.Composite;
import com.patterns.operational.composite.Leaf;
import com.patterns.operational.decorator.HouseDecorator;
import com.patterns.operational.decorator.NormalHouse;
import com.patterns.operational.decorator.Villa;
import com.patterns.operational.observer.NewspaperClient;
import com.patterns.operational.observer.NewspaperFakt;

import java.util.Random;

public class Main {
    public static void main(String[] args) throws CloneNotSupportedException {
       /* ExternalMachineConnection machineConnection = ExternalMachineConnection.getINSTANCE();
        System.out.print(machineConnection.getData());
        SQLConnection sqlConnection = SQLConnection.getINSTANCE();
        System.out.print(sqlConnection.getData());
        MemoryConnection memoryConnection = MemoryConnection.INSTANCE;
        System.out.print(memoryConnection.getData());*/

/*        com.patterns.creational.builder.standard.CarBuilder builder =
                new com.patterns.creational.builder.standard.CarBuilder();
        builder.setBody(Body.Crossover);
        builder.setColor(Color.Black);
        builder.setDrive(Drive.Diesel);
        builder.setMark(Mark.BMW);
        Car bmwX3 = builder.build();
        System.out.print(bmwX3.toString());

        Car bmwX5 = new com.patterns.creational.builder.fluentApi.CarBuilder()
                .setBody(Body.Crossover)
                .setColor(Color.Black)
                .setDrive(Drive.Diesel)
                .setMark(Mark.BMW).build();

        System.out.print(bmwX5.toString());*/



        /*Car mayAudi = new AudiFactory().get(Month.May);
        System.out.print(mayAudi.toString());

        MonthFactory<Car> summerFactory = new PolishFactory().get(PartOfYear.Summer);
        Car summerCar = summerFactory.get(Month.January);
        System.out.println(summerCar.toString());

        System.out.println();
        System.out.println("Normal house");
        NormalHouse normalHouse = new NormalHouse();
        normalHouse.Enter();
        normalHouse.Sleep();
        normalHouse.Exit();

        System.out.println();
        System.out.println("Vacant");
        Vacant vacant = new Vacant(normalHouse);
        vacant.Enter();
        vacant.Sleep();
        vacant.Exit();
        vacant.Demolish();

        System.out.println();
        System.out.println("Villa");
        Villa villa = new Villa(normalHouse);
        villa.Enter();
        villa.Sleep();
        villa.Swim();
        villa.Exit();


        System.out.println();
        System.out.println("Explorer star journey");
        Random r = new Random();
        final int CAMP_COUNT = 10;

        ExplorationJournal journal = new ExplorationJournal();
        Explorer explorer = new Explorer();

        for(int campIndex =0; campIndex<CAMP_COUNT;campIndex++) {
            switch (r.nextInt() % 4) {
                case 0:
                    journal.makeNote(new MoveUpCommand(explorer));
                    break;
                case 1:
                    journal.makeNote(new MoveDownCommand(explorer));
                    break;
                case 2:
                    journal.makeNote(new MoveLeftCommand(explorer));
                    break;
                default:
                    journal.makeNote(new MoveRightCommand(explorer));
                    break;
            }
        }

        System.out.println();
        System.out.println("Explorer print his book based on his journal");
        journal.print();*/

        // ----------------------------------------------

        PersonalComputer pc = new CaseBuilder()
                .setCase(Case.HighTower)
                .setRam(RAM.ADATA)
                .setDrive(Drive.HDD)
                .setProcessor(Processor.AMD)
                .setMainboard(Mainboard.Asus)
                .setGraphicalCard(GraphicsCard.Zotac)
                .build();
        // GraphicsCard -> Mainboard -> Processor -> Drive -> RAM -> Case ->

        // ----------------------------------------------

        ConcreteFactory1 f1 = new ConcreteFactory1();
        ConcreteFactory2 f2 = new ConcreteFactory2();

        AbstractFactory af1 = new ConcreteFactory1();
        AbstractFactory af2 = new ConcreteFactory2();


        AbstractProductA a1 = f1.createProductA();
        AbstractProductB b1 = f1.createProductB();

        AbstractProductA a2 = f2.createProductA();
        AbstractProductB b2 = f2.createProductB();


        // ----------------------------------------------


        CellPrototype cellPrototype = new CellPrototype(10);
        CellPrototype cellClone = cellPrototype.cloneCell();

//        System.out.println(cellClone.equals(cellPrototype));
        cellClone.setMass(20);


        // ----------------------------------------------

/*

        List<Person> personList = new ArrayList<>();
        personList.add(new WarriorAdapter());
        personList.add(new WarriorAdapter());
        personList.add(new WizzardAdapter());
        personList.add(new WarriorAdapter());
        personList.add(new WarriorAdapter());


        for (Person p : personList) {
            p.attack();
        }
*/


        // ----------------------------------------------


        Rectangle rectangle = new Rectangle(new Paint());
//        rectangle.draw();

        Circle circle = new Circle(new Photoshop());
//        circle.draw();

        // ----------------------------------------------
/*

        Component component1 = new Component(1);
        Component component2 = new Component(2);
        Composite composite1 = new Composite(3);

        composite1.addComponent(component1);

        Composite composite2 = new Composite(4);
        composite2.addComponent(composite1);
        composite2.addComponent(component2);
*/

        Composite composite = new Composite(3);
        Leaf leaf = new Leaf(1);

        composite.addComponent(leaf);
        composite.addComponent(new Composite(2));


        // ----------------------------------------------

/*

        NormalHouse normalHouse = new NormalHouse();
        normalHouse.Enter();
        normalHouse.Sleep();
        normalHouse.Exit();

        System.out.println("------- villa ------------");

        Villa villa = new Villa(new NormalHouse());
        villa.Enter();
        villa.Swim();
        villa.Sleep();
        villa.Exit();


        System.out.println("------- decorated ------------");

        HouseDecorator decorator = new HouseDecorator(normalHouse);
        decorator.Enter();
        decorator.Sleep();
        decorator.Exit();

*/


        // ----------------------------------------------


/*
        CarDecorator carDecorator = new CarDecorator(new AverageCar());
        carDecorator.enter();
        carDecorator.drive();
        carDecorator.stop();
*/

/*
        Bmw bmw = new Bmw();
        bmw.enter();
        bmw.drive();
        bmw.stop();
        bmw.makeSoup();

        System.out.println("-- trucker --");

        Car bmwTruck = new BmwTruck();
        bmwTruck.enter();
        bmwTruck.drive();
        bmwTruck.stop();
*/

        // ----------------------------------------------

/*

        System.out.println("Explorer starts journey");
        Random r = new Random();
        final int CAMP_COUNT = 10; // max 10 przystankow

        ExplorationJournal journal = new ExplorationJournal();
        Explorer explorer = new Explorer();

        for(int campIndex =0; campIndex<CAMP_COUNT;campIndex++) {
            switch (r.nextInt() % 4) {
                case 0:
                    journal.makeNote(new MoveUpCommand(explorer));
                    break;
                case 1:
                    journal.makeNote(new MoveDownCommand(explorer));
                    break;
                case 2:
                    journal.makeNote(new MoveLeftCommand(explorer));
                    break;
                default:
                    journal.makeNote(new MoveRightCommand(explorer));
//                    break;
            }
        }

        System.out.println();
        System.out.println("Explorer print his book based on his journal");
        journal.print();

*/


        NewspaperClient pawel = new NewspaperClient("pawel");
        NewspaperClient agnieszka = new NewspaperClient("agnieszka");
        NewspaperClient marian = new NewspaperClient("marian");
        NewspaperClient ola = new NewspaperClient("ola");
        NewspaperClient adam = new NewspaperClient("adam");

        NewspaperFakt fakt = new NewspaperFakt();

        fakt.attach(pawel);
        fakt.attach(adam);
        fakt.attach(agnieszka);

//        fakt.promote();


    }
}